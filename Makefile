JC = javac
JE = java
JR_FLAGS = -cp .:javax.mail.jar:commons-email-1.4.jar:jsoup-1.8.3.jar

# -Xlint:unchecked

all: compile

compile:
	javac -cp .:javax.mail.jar:commons-email-1.4.jar:jsoup-1.8.3.jar *.java


sample: compile
	$(JE) $(JR_FLAGS) Filter sampletrain/ sampletrain/ham3.txt

run: compile
	$(JE) $(JR_FLAGS) Filter train/ train/ham56.txt

push: 
	git add --all
	git commit -m "automatic commit"
	git push

clean:
	rm *.class
