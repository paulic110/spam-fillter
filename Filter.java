import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.*;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.*;


import javax.mail.internet.MimeMultipart; 
import javax.mail.internet.MimeMessage;
import javax.mail.BodyPart;
import javax.mail.Session;
import javax.mail.MessagingException;
import javax.mail.Message;

  
public class Filter {

  // PRIVATE 
  // private final Path fFilePath;
  private final static Charset ENCODING = StandardCharsets.UTF_8;  
  private  Map<String, Integer> testMap = new HashMap<String, Integer>();
  private Map<String, Integer> spamMap = new HashMap<String, Integer>();
  private Map<String, Integer> hamMap = new HashMap<String, Integer>();
  private List<String> featuresTest = new ArrayList<String>();
  private List<String> featuresHam = new ArrayList<String>();
  private List<String> featuresSpam = new ArrayList<String>();

  private Integer hamCount, spamCount;

  private BayseClassifier bc;

  public static void main(String[] aArgs) throws IOException, MessagingException {
    String text;
    Filter parser = new Filter();
    String directtory = aArgs[0];
    String testFile = aArgs[1];
    


    parser.trainClassifier(directtory);
    parser.bc.writeToFile();
    // parser.bc = new BayseClassifier();
    // parser.bc.readFiles();

    // parser.tenFoldValidation(directtory);

    parser.testBunch(directtory, true, 1, 2400);

    // boolean result = parser.bc.classify(Paths.get(testFile));
    // if(result) log("spam");
    // else log("ham");

  }


  private final void tenFoldValidation(String dirPath){

    spamCount = bc.getCount("spam");
    hamCount = bc.getCount("ham");

    Integer startSpam = 1;
    Integer lastSpam = 0;
    Integer startHam = 1;
    Integer lastHam = 0;


    for(int i=0; i<10; i++){
      lastSpam = (i+1)* spamCount/10;
      lastHam = (i+1)* hamCount/10 ;

      trainBunch(dirPath, startSpam, lastSpam, startHam, lastHam);

      int noSpams = testBunch(dirPath, true, startSpam, lastSpam);
      int noHams = lastSpam - startSpam - noSpams;

      int noSpams2 = testBunch(dirPath, false, startHam, lastHam);
      int noHams2 = lastHam - startHam - noSpams2;

      log( (float)(noSpams + noHams2)/((lastSpam - startSpam)+(lastHam - startHam)) );


      startHam = lastHam;
      startSpam = lastSpam; 
    }
  }


  private final int testBunch(String dirPath, boolean spam, Integer startCount, Integer endCount){
    boolean result;
   
    int spamsDetected = 0;
    int hamsDetected = 0;
    int i = startCount;

    String text = new String();
    String mailType = (spam) ? "spam" : "ham";
    String filePath = dirPath + mailType +String.valueOf(i) + ".txt";

    File file = new File(filePath);
    // bc = new BayseClassifier();
    while(file.exists() && i < endCount){
      // log(filePath);
      result = bc.classify(Paths.get(filePath));
      if(result) spamsDetected++;
      else hamsDetected++;

      i++;
      // if(result && !spam) log(i);

      filePath = dirPath + mailType+ String.valueOf(i) + ".txt";
      file = new File(filePath);
    }

    log("In " + mailType + " from " + startCount + " to " + endCount +" have been found : ");
    log("spams :" + spamsDetected);
    log("hams :" + hamsDetected);

    return spamsDetected;

  }

  private final void trainBunch(String dirPath, Integer startSpam, Integer lastSpam, Integer startHam, Integer lastHam){
    bc = new BayseClassifier(0,0);

    int i = 1;
    String filePath = dirPath + "ham" + String.valueOf(i) + ".txt";
    File file = new File(filePath);
    while(file.exists()){
      if(!(i>=startHam && i<lastHam)){
        try{
          bc.train(Paths.get(filePath), false, i - startHam);
        }catch(IOException ex){}catch(MessagingException e){}  
      }

      i++;
      filePath = dirPath + "ham"+ String.valueOf(i) + ".txt";
      file = new File(filePath);
    }

    i = 1;
    filePath = dirPath + "spam"+ String.valueOf(i) + ".txt";
    file = new File(filePath);
    while(file.exists()){
      if(!(i>= startSpam && i< lastSpam)){
        try{
          bc.train(Paths.get(filePath), true, i - startSpam);
        }catch(IOException ex){}catch(MessagingException e){}
      }

      i++;
      filePath = dirPath + "spam"+ String.valueOf(i) + ".txt";
      file = new File(filePath);
    }

    try {
      bc.removeStopWords();
    } catch ( Exception e) {};
    // bc.showStats();
  }


  // public final String getMessage(Path fFilePath) throws IOException, MessagingException, UnsupportedEncodingException {
  //   String text = "";
  //   MimeMessage mm;

  //   try ( Scanner scanner = new Scanner(fFilePath, ENCODING.name())) {
  //     text = scanner.useDelimiter("\\Z").next();
  //     Session session = Session.getDefaultInstance(new Properties());
  //     ByteArrayInputStream is = new ByteArrayInputStream(text.getBytes());
  //     mm = new MimeMessage(session, is);
  //     //if ( mm.getContentType().toString().split(";")[0].toLowerCase().contains("text/plain")) {
  //       // log(mm.getSubject());
  //       Object obj = mm.getContent();
  //       if ( obj.getClass().equals(String.class) && ((String)obj).length() != 0) {
  //         text = (String) mm.getContent();
  //         text = text.replaceAll("(<.*?>)", " ")
  //                    .replaceAll("(&.*;)"," ")
  //                    .replaceAll("[\\d*]", " ")
  //                    .replaceAll("^[0-9]*$", " ")
  //                    .replaceAll("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})", " ")
  //                    .replaceAll("[^a-zA-Z0-9 ]"," ")
  //                    .replaceAll("(\\s+)", " ");
  //       } else if ( obj.getClass().equals(MimeMultipart.class)) {
  //         text = "";
  //       } else {
  //         text = text;
  //       }
  //   }
  //   return text;
  // }

  public final String getMessage(Path fFilePath) throws IOException, MessagingException, UnsupportedEncodingException {
    String text = "";
    MimeMessage mm;

    try ( Scanner scanner = new Scanner(fFilePath, ENCODING.name())) {
      text = scanner.useDelimiter("\\Z").next();
      Session session = Session.getDefaultInstance(new Properties());
      ByteArrayInputStream is = new ByteArrayInputStream(text.getBytes());
      mm = new MimeMessage(session, is);
      //if ( mm.getContentType().toString().split(";")[0].toLowerCase().contains("text/plain")) {
        // log(mm.getSubject());

      Object obj = mm.getContent();
      text = processMultipart(obj, text);
    }
    return text;
  }

  private String processMultipart(Object message, String text) throws MessagingException, IOException{
    String result = "";
    if ( message.getClass().equals(String.class) && ((String)message).length() != 0) {
          result = (String) message;
          result = result.replaceAll("(<.*?>)", " ")
                     .replaceAll("(&.*;)"," ")
                     .replaceAll("[\\d*]", " ")
                     .replaceAll("^[0-9]*$", " ")
                     .replaceAll("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})", " ")
                     .replaceAll("[^a-zA-Z0-9 ]"," ")
                     .replaceAll("(\\s+)", " ");
    } else if ( message.getClass().equals(MimeMultipart.class)) {
      log("intru");
      MimeMultipart obj = (MimeMultipart) message;
      for(int i=0;i< 1; i++){
        log(obj.getBodyPart(i).getContent());
        // result += processMultipart(obj.getBodyPart(i).getContent(), text);
      }
      // log(result);
    } else {
      result = text;
      log(message.getClass());
    }
    return result;
  }

  private int getNumberOfFiles(String type, String dirPath) throws IOException{
    int i = 1;
    String filePath = dirPath + type + i+".txt";
    File file = new File(filePath);
    while(file.exists()) {
      i++;
      filePath = dirPath + type + i + ".txt";
      file = new File(filePath);
    }
    return i-1;
  }

  private final String trainClassifier(String dirPath) throws IOException, MessagingException{
    int i = 1;

    String filePath = dirPath + "ham" + String.valueOf(i)+".txt";
    File file = new File(filePath);
    bc = new BayseClassifier(getNumberOfFiles("ham", dirPath), getNumberOfFiles("spam",dirPath));
    // bc.setLimits(getNumberOfFiles("ham", dirPath), getNumberOfFiles("spam",dirPath));
    while(file.exists()){
      bc.train(Paths.get(filePath), false, i - 1);
      
      i++;
      filePath = dirPath + "ham"+ String.valueOf(i) + ".txt";
      file = new File(filePath);
    }

    i = 1;
    filePath = dirPath + "spam"+ String.valueOf(i) + ".txt";
    file = new File(filePath);
    while(file.exists()){
      try{
        bc.train(Paths.get(filePath), true, i-1);
      } catch (UnsupportedEncodingException e){ log(e);}

      i++;
      filePath = dirPath + "spam"+ String.valueOf(i) + ".txt";
      file = new File(filePath);
    }

    try {
      // bc.removeStopWords();
    }catch(Exception e) {};
    return null;
  }


  public final String processWordByWord(List<String> features , Map<String, Integer> featuresMap, Path fFilePath) throws IOException{
    List<String> localFeatures = new ArrayList<String>();
    String word = "";
    try (Scanner scanner =  new Scanner(fFilePath, ENCODING.name())){
      while (scanner.hasNext()){
        word = scanner.next();
        Integer value = featuresMap.get(word);
        if (!localFeatures.contains(word)) {
          if( value!= null ){
            featuresMap.put(word, value + 1);
          }
          else{
            featuresMap.put(word, 1);
            features.add(word);
          }
          localFeatures.add(word);
        }
      }      
    }
    return null;
  }
  
  private Boolean wordCheck(String word){
    if(word.contains("<") ||
       word.contains(">") ||
       word.contains("[") ||
       word.contains("/")
       ){
      return false;
    }
    return true;
  }
  
  private static void log(Object aObject){
    System.out.println(String.valueOf(aObject));
  }
  
  private String quote(String aText){
    String QUOTE = "'";
    return QUOTE + aText + QUOTE;
  }


} 