import java.util.*;

public class Stash {
	private List<String> tags;

	public Stash() {
		tags = new ArrayList<String>();
	}

	public List<String> getStash(){
		return tags;
	} 

	public void push(String newtag){
		tags.add(newtag);
	}

	public void pop(){
		if(tags.size()>0)
			tags.remove(tags.size()-1);
	}

	public String getLast(){
		if(tags.size()>0){
			return tags.get(tags.size()-1);
		}
		return null;
	}
}