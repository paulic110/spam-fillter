import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.io.IOException;

import javax.mail.internet.MimeMultipart; 
import javax.mail.internet.MimeMessage;
import javax.mail.BodyPart;
import javax.mail.Session;
import javax.mail.MessagingException;
import javax.mail.Message;
import org.apache.commons.mail.util.MimeMessageParser;
import org.jsoup.Jsoup;

// import org.apache.commons.codec.binary.Base64;
// import org.apache.commons.io.IOUtils;
import com.sun.mail.util.BASE64DecoderStream;

public class BayseClassifier {

  private Map<String, List<Integer[]>> m;
  private Map<String, Integer> model;

  private Map<String, Integer> spamMap;
  private Map<String, Integer> hamMap;

  private Integer spamCount;
  private Integer hamCount;

  private List<String> featuresHam;
  private List<String> featuresSpam;
  private String key;
  private Integer max;
  private Integer totalWordsInSpam;
  private Integer totalWordsInHam;

  private final static Charset ENCODING = StandardCharsets.UTF_8;  

  public BayseClassifier(int no_hams, int no_spams) {
    model = new HashMap<String, Integer>();
    spamMap = new HashMap<String, Integer>();
    hamMap = new HashMap<String, Integer>();
    featuresHam = new ArrayList<String>();
    featuresSpam = new ArrayList<String>();
    spamCount = no_spams;
    hamCount = no_hams;
    m = new HashMap<String, List<Integer[]>>();
    max = 0;
    key = "";
    totalWordsInSpam = 0;
    totalWordsInHam = 0;
  }

  public BayseClassifier() {

  }

  public final void writeToFile(){
    List<Integer> counts = new ArrayList<Integer>();
    counts.add(spamCount);
    counts.add(hamCount);
    counts.add(totalWordsInSpam);
    counts.add(totalWordsInHam);
    try(
      OutputStream count_file = new FileOutputStream("counts.ser");
      OutputStream count_buffer = new BufferedOutputStream(count_file);
      ObjectOutput count_output = new ObjectOutputStream(count_buffer);

      OutputStream spamMap_file = new FileOutputStream("spamMap.ser");
      OutputStream spamMap_buffer = new BufferedOutputStream(spamMap_file);
      ObjectOutput spamMap_output = new ObjectOutputStream(spamMap_buffer);

      OutputStream hamMap_file = new FileOutputStream("hamMap.ser");
      OutputStream hamMap_buffer = new BufferedOutputStream(hamMap_file);
      ObjectOutput hamMap_output = new ObjectOutputStream(hamMap_buffer);


      OutputStream featuresHam_file = new FileOutputStream("featuresHam.ser");
      OutputStream featuresHam_buffer = new BufferedOutputStream(featuresHam_file);
      ObjectOutput featuresHam_output = new ObjectOutputStream(featuresHam_buffer);

      OutputStream featuresSpam_file = new FileOutputStream("featuresSpam.ser");
      OutputStream featuresSpam_buffer = new BufferedOutputStream(featuresSpam_file);
      ObjectOutput featuresSpam_output = new ObjectOutputStream(featuresSpam_buffer);
    ){
      count_output.writeObject(counts);

      spamMap_output.writeObject(spamMap);
      featuresSpam_output.writeObject(featuresSpam);

      hamMap_output.writeObject(hamMap);
      featuresHam_output.writeObject(featuresHam);
    } catch(IOException ex){
      log(ex);
    }
  }

  public final void readFiles(){
    try(

      InputStream count_file = new FileInputStream("counts.ser");
      InputStream count_buffer = new BufferedInputStream(count_file);
      ObjectInput count_input = new ObjectInputStream(count_buffer);

      InputStream spamMap_file = new FileInputStream("spamMap.ser");
      InputStream spamMap_buffer = new BufferedInputStream(spamMap_file);
      ObjectInput spamMap_input = new ObjectInputStream(spamMap_buffer);

      InputStream hamMap_file = new FileInputStream("hamMap.ser");
      InputStream hamMap_buffer = new BufferedInputStream(hamMap_file);
      ObjectInput hamMap_input = new ObjectInputStream(hamMap_buffer);


      InputStream featuresHam_file = new FileInputStream("featuresHam.ser");
      InputStream featuresHam_buffer = new BufferedInputStream(featuresHam_file);
      ObjectInput featuresHam_input = new ObjectInputStream(featuresHam_buffer);

      InputStream featuresSpam_file = new FileInputStream("featuresSpam.ser");
      InputStream featuresSpam_buffer = new BufferedInputStream(featuresSpam_file);
      ObjectInput featuresSpam_input = new ObjectInputStream(featuresSpam_buffer);
    ){
      List<Integer> counts = (List<Integer>)count_input.readObject();
      spamCount = counts.get(0);
      hamCount = counts.get(1);
      totalWordsInSpam = counts.get(2);
      totalWordsInHam = counts.get(3);

      featuresSpam = (List<String>)featuresSpam_input.readObject();
      featuresHam = (List<String>)featuresHam_input.readObject();

      spamMap = (HashMap<String,Integer>)spamMap_input.readObject();
      hamMap = (HashMap<String,Integer>)hamMap_input.readObject();
    }catch(ClassNotFoundException ex){
      log(ex);
    } 
    catch(IOException ex){
      log(ex);  
    }
  }

  public boolean classify(Path filePath){

    String text = new String();
    String subject = new String();
    // laplaceCorrection();
    try{
      text = getMessage(filePath, true);
      // subject = getMessage(filePath, false);
    }catch(IOException e){}catch(MessagingException e){} 

    // if(subject.contains("RE:")) return true;

    Double p_s = new Double(spamCount)/(spamCount+hamCount);
    Double p_h = 1.0f - p_s;

    // Double p_word_s = 1.0d, p_word_h = 1.0d;
    Integer value;
    Double hamPow = 1.0d;
    // log(text);
    String[] mata = text.split(" ");
    Integer len = mata.length;

    double p_word_s = 0.0d, p_n_word_s = 0.0d, p_word_h = 0.0d , p_n_word_h =0.0d;

    // log("length "+ len);

    for(String word : mata){
      p_word_h += ((value = hamMap.get(word))  != null) ?  Math.log10(((double)value + 1.0d)/(double)(totalWordsInHam  + len)) : - Math.log10((totalWordsInHam  + len));
      p_word_s += ((value = spamMap.get(word)) != null) ?  Math.log10(((double)value + 1.0d)/(double)(totalWordsInSpam + len)) : - Math.log10((totalWordsInSpam + len));
      // if (hamMap.get(word) != null) log((hamMap.get(word) + 1.0d) + " " + totalWordsInHam);
    }

    Double spamProb = p_word_s;
    Double hamProb = p_word_h;
    
    Double finalProb = Math.log10((double)(totalWordsInSpam+1)/(double)(totalWordsInHam+1)) + (spamProb - hamProb);

    return (finalProb > 0);
  }

  private void laplaceCorrection(){
    for (Map.Entry<String, Integer> entry : spamMap.entrySet()) {
      if(entry.getValue() > max) {
        max = entry.getValue();
        key = entry.getKey();
        log(key + " == "+max);
      }
    }
  }

  
  public void showStats(){
    for(String word : featuresSpam){
      if(spamMap.get(word)!= null & hamMap.get(word)!= null){
        log(word);
      }
    }
  }

  public void train(Path filePath, boolean isSpam, int mailIndex)throws IOException, MessagingException, UnsupportedEncodingException {
      if(isSpam) {
        trainOne(getMessage(filePath, true), featuresSpam, spamMap, isSpam, mailIndex);

      } else {
        trainOne(getMessage(filePath, true), featuresHam, hamMap, isSpam, mailIndex);
      }
  }


  public final void setLimits(int number_ham, int number_spam) {
    hamCount = number_ham;
    spamCount = number_spam;
    log(hamCount + " " + spamCount);
  }
  public final String getMessage(Path fFilePath, boolean message) throws IOException, MessagingException, UnsupportedEncodingException {
    String text = "";
    MimeMessage mm;
    // log("---------------------------------------------------------------------------"+fFilePath);
    try ( Scanner scanner = new Scanner(fFilePath, ENCODING.name())) {
      text = scanner.useDelimiter("\\Z").next();
      Session session = Session.getDefaultInstance(new Properties());
      ByteArrayInputStream is = new ByteArrayInputStream(text.getBytes());
      mm = new MimeMessage(session, is);
      //if ( mm.getContentType().toString().split(";")[0].toLowerCase().contains("text/plain")) {
        // log(mm.getSubject());
      if(!message) {
        // log(mm.getSubject());
        Object obj = mm.getSubject();
        if(obj != null && obj.getClass().equals(String.class) && ((String)obj).length() !=0)  text = (String)mm.getSubject();
          else if(obj!= null && obj.getClass().equals(MimeMultipart.class)) text = "";
          else text = "";
          return text;
      }
      else{
        Object obj = mm.getContent();
        MimeMessageParser parser = new MimeMessageParser(mm);
        try {
          parser.parse();
          if(parser.hasHtmlContent()){
            text += Jsoup.parse(parser.getHtmlContent()).text() + "\n";
          }
          if(parser.hasPlainContent()) {
            text += parser.getPlainContent();
          }
          if(parser.hasAttachments()) {

          }
          // }//log(Jsoup.parse(parser.getHtmlContent()).text());
          // else if (parser.hasPlainContent()) {}
          // else {} //log(parser.getAttachmentList() + " " + fFilePath);
          // log(parser.isMultipart());
        }catch(Exception e) {};
        // text = processMultipart(obj, text);
        // if ( obj.getClass().equals(String.class) && ((String)obj).length() != 0) {
        //   text = (String) mm.getContent();
        //   text = text.replaceAll("(<.*?>)", " ")
        //              .replaceAll("(&.*;)"," ")
        //              .replaceAll("[\\d*]", " ")
        //              .replaceAll("^[0-9]*$", " ")
        //              .replaceAll("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})", " ")
        //              .replaceAll("[^a-zA-Z0-9 ]"," ")
        //              .replaceAll("(\\s+)", " ");
        // } else if ( obj.getClass().equals(MimeMultipart.class)) {
        //   log(text);
        //   text = "";
        // } else {
        //   text = text;
        // }
      }

    }
    return cleanup(text);
  }

  private String cleanup(String text) {
    return text
             .replaceAll("(\\s+)", " ")
             .replaceAll("(<style.*?>.*?</style>)","")
             .replaceAll("(<.*?>)", " ")
             .replaceAll("(&.*;)"," ")
             .replaceAll("[\\d*]", " ")
             .replaceAll("^[0-9]*$", " ")
             .replaceAll("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})", " ")
             .replaceAll("[^a-zA-Z0-9 ]"," ")
             .replaceAll("(\\s+)", " ");
  }

  private String processMultipart(Object message, String text) throws MessagingException, IOException{
    String result = "";
    if ( message.getClass().equals(String.class) && ((String)message).length() != 0) {
      // log("no multipart");
          result = (String) message;
          result = result
                     .replaceAll("(\\s+)", " ")
                     .replaceAll("(<style.*?>.*?</style>)","")
                     .replaceAll("(<.*?>)", " ")
                     .replaceAll("(&.*;)"," ")
                     .replaceAll("[\\d*]", " ")
                     .replaceAll("^[0-9]*$", " ")
                     .replaceAll("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})", " ")
                     .replaceAll("[^a-zA-Z0-9 ]"," ")
                     .replaceAll("(\\s+)", " ");
    } else if ( message.getClass().equals(MimeMultipart.class)) {
      // log("intru");
      MimeMultipart obj = (MimeMultipart) message;
      for(int i=0;i< obj.getCount(); i++){
        result += processMultipart(obj.getBodyPart(i).getContent(), text);
      }
      // log(result);
    // } else if (message instanceof BASE64DecoderStream) {
      // BASE64DecoderStream base64DecoderStream = (BASE64DecoderStream) message;
      // byte[] byteArray = IOUtils.toByteArray(base64DecoderStream);
      // byte[] encodeBase64 = Base64.encodeBase64(byteArray);
      // String base64Content = new String(encodeBase64, "UTF-8");
      // log(base64DecoderStream);
      // base64Content[1] = getContentTypeString(part);
    } else {
      // log(text);
      result = text;
      log(message.getClass());
    }
    // log(result);
    return result;
  }

  private void trainOne(String file, List<String> features , Map<String, Integer> featuresMap, boolean isSpam, int mailIndex) {
    List<String> localFeatures = new ArrayList<String>();
    for(String word : file.split("(\\s+)")){
      Integer value = featuresMap.get(word);
      
      if ((word.length() != 0)) {
        if( value != null ){
          featuresMap.put(word, value + 1);
        }
        else {
          Integer[] current_spam = new Integer[spamCount];
          Integer[] current_ham = new Integer[hamCount];
          List<Integer[]> current = new ArrayList<Integer[]>();
          current.add(current_spam);
          current.add(current_ham);
          // List<Integer> current_spam = new ArrayList<Integer>(spamCount);
          // List<Integer> current_ham = new ArrayList<Integer>(hamCount);
          // List<Integer>[] current = new ArrayList[2];
          // current[0] = current_spam;
          // current[1] = current_ham;
          // log(spamCount);
          // log(current_spam.length);
          // log(current[0].size());
          m.put(word, current);
          featuresMap.put(word, 1);
          features.add(word);
        }
        int type = (isSpam) ? 0 : 1;
        List<Integer[]> modify = m.get(word);
        if ( modify.get(type)[(mailIndex)] != null ) {
          modify.get(type)[mailIndex] += 1;
        } else {
          modify.get(type)[mailIndex] = 1;
        }
        m.put(word, modify);
        if (isSpam) totalWordsInSpam++; else totalWordsInHam++;
        localFeatures.add(word);
      }
    }      
  }

  public Integer getCount(String key) {
    if (key.equals("spam")) {
      return spamCount;
    } else if ( key.equals("ham")) {
      return hamCount;
    } else return 0;
  }

  public final void removeStopWords() throws IOException{
    try(Scanner scanner = new Scanner(Paths.get("stopWords.txt"), ENCODING.name())){
      while (scanner.hasNext()){
        String word = scanner.next();
        spamMap.remove(word);
        hamMap.remove(word);
      }
    }

    // String loWord;  
    // for(String woord : featuresSpam){
    //   if(spamMap.get(woord)!= null && spamMap.get(loWord)!= null){
    //     spamMap.put(loWord, spamMap.get(woord) + spamMap.get(loWord));
    //     spamMap.remove(woord);
    //   }
    // }

     for(String word : featuresSpam){
      if(spamMap.get(word)!= null & hamMap.get(word)!= null){
        // log("for word : " + word);
        // log("spamProb is : " + (double)spamMap.get(word)/spamCount);
        // log("hamProb is : " + (double)hamMap.get(word)/hamCount);
        Double prob = ((double)spamMap.get(word)/spamCount)/((double)hamMap.get(word)/hamCount);
        Double absProb = Math.abs(prob - 1);
        // log("prob is : " + prob + "and abs value is " + absProb );

        // if(absProb < 0.05){
        //   spamMap.remove(word);
        //   hamMap.remove(word);
        // }  

      }
    }
  }



  private static void log(Object aObject){
    System.out.println(String.valueOf(aObject));
  }
}