// import java.io.*;
// import java.util.Properties;
// import java.util.Scanner;

// // import org.jsoup.Jsoup;

// import javax.mail.*;
// import javax.mail.internet.MimeMessage;
// import javax.mail.internet.MimeMultipart;
// import javax.mail.util.SharedByteArrayInputStream;

// public class Parser 
// {	

	
// 	//gets the whole email in a single string
// 	private static String someFunction(File inputFile)
// 	{
		
// 		String content=new String("");
// 		try {
// 			content = new Scanner(inputFile).useDelimiter("\\Z").next();
// 		} catch (Exception e) {}// System.out.println("Something went wrong with reading the file asdf "+inputFile.getName()+"\ne: "+e);}
		
// 		return content;
// 	}
	
// 	//gets a String from a MimeMultipart message
// 	public static String getTextFromMime(MimeMultipart message) throws MessagingException, IOException
// 	{
		
// 		String output=new String("");
// 		for (int i=0;i<message.getCount() ;i++)
// 		{	
// 			Object temp =  message.getBodyPart(i).getContent();
			
// 			if (temp.getClass().equals(String.class)) //if bodypart contains string
// 				output+=(String)temp;
// 			else if (temp.getClass().equals(MimeMultipart.class)) //if bodypart contains more multiparts
// 				output=output+getTextFromMime((MimeMultipart)temp);
// 			else //just make it a string
// 				output=output+temp.toString();
// 		}
// 		return output;
// 	}
	
// 	//gets the body of the given email as a String
// 	public static String getBody(String fileName, File inputFile)
// 	{
// 		Object content=null;
// 		String textMessage="";
// 		if (inputFile==null)
// 		{
// 			inputFile = new File(fileName);
// 		}
// 		String emailString = new String( someFunction(inputFile));
// 		//System.out.println("emailString: "+emailString);
// 		try
// 		{	
			
			
// 			Session s = Session.getDefaultInstance(new Properties());
// 			ByteArrayInputStream is = new ByteArrayInputStream (emailString.getBytes());
// 			MimeMessage message = new MimeMessage(s,is);
// 			try
// 			{
// 				content=message.getContent();
// 			}catch(Exception ex){System.out.println("Unsupported charset: "+ex);}
			
// 			if(content.getClass().equals(String.class))
// 				textMessage = (String)content;
// 			else if (content.getClass().equals(MimeMultipart.class))
// 				textMessage=getTextFromMime((MimeMultipart)content);
// 			else
// 			{	
// 				InputStreamReader isr=new InputStreamReader((SharedByteArrayInputStream)content);
// 				char c;
// 				int i;
// 				while ((i=isr.read())!=-1)
// 				{
// 					c=(char)i;
// 					textMessage=textMessage+c;
// 				}
// 				isr.close();//System.out.println("wtf is class" + content.getClass().toString());
// 			}
			
// 		} catch (Exception ex) {}
		
// 		//textMessage=Jsoup.parse(textMessage).text();
// 		textMessage=removeCharacters(textMessage);
// 		//System.out.println(textMessage);
		
			
// 		return textMessage;
// 	}
	
// 	//removes white space and non-alphabetical characters from a string
// 	public static String removeCharacters(String str)
// 	{
// 		str=str.replaceAll("[^a-zA-Z]"," ");
// 		str=str.replaceAll("\\s+","+");
// 		//str=str.replace('+',' ');
// 		return str;
// 	}
	
// 	//generates parsed train files to be used in the 10 fold cross validation
// 	public void processTrainFolder(String folderName)
// 	{
		
// 		String outputName=null;
// 		String inputName=null;
// 		File trainFolder = new File(folderName);
// 		File[] emails = trainFolder.listFiles();
		
// 		int n = emails.length;
// 		for (int i=0;i<n;i++)
// 		{
// 			//System.out.println(i);
// 			if(emails[i].isFile())
// 			{
// 				inputName=emails[i].getName();
// 				String message = getBody(null,emails[i]);
// 				if (inputName.startsWith("ham"))
// 					outputName = "hamTrain"+inputName.substring(3,inputName.length());
// 				else
// 					outputName="spamTrain"+inputName.substring(4,inputName.length());
// 				PrintWriter writer=null;
// 				try	{
// 					writer = new PrintWriter(outputName, "UTF-8");
// 				} catch (Exception ex){}
					
// 				writer.println(message);
// 				writer.close();
// 			}
// 		}
		
// 	}
// 	/*public static void main(String[] args) 
// 	{
		
// 		Parser Program = new Parser();
// 		Program.processTrainFolder("train");
		
// 		return;
// 	}*/
	
// }
